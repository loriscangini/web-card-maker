import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CardCreationComponent } from './components/card-creation/card-creation.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import 'froala-editor/js/plugins.pkgd.min.js';
import 'froala-editor/js/third_party/font_awesome.min';
import {MatSliderModule} from '@angular/material/slider';
import {MatCardModule} from '@angular/material/card';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatSelectModule} from '@angular/material/select';
import {MatDialogModule} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import { OptionsGeneralComponent } from './components/options/options-general/options-general.component';
import { OptionsImageComponent } from './components/options/options-image/options-image.component';
import { OptionsTextComponent } from './components/options/options-text/options-text.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FroalaWrapperComponent } from './components/froala-wrapper/froala-wrapper.component';
import { AttributeSelectorComponent } from './components/attribute-selector/attribute-selector.component';
import { AttributeSelectorDialogComponent } from './components/attribute-selector/attribute-selector-dialog/attribute-selector-dialog.component';
import { StarsComponent } from './components/stars/stars.component';

@NgModule({
  declarations: [
    AppComponent,
    CardCreationComponent,
    OptionsGeneralComponent,
    OptionsImageComponent,
    OptionsTextComponent,
    FroalaWrapperComponent,
    AttributeSelectorComponent,
    AttributeSelectorDialogComponent,
    StarsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgbModule,
    FormsModule,
    HttpClientModule,
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot(),
    MatSliderModule,
    MatCardModule,
    MatCheckboxModule,
    MatSelectModule,
    MatDialogModule,
    MatButtonModule,
    FontAwesomeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
