import { Component } from '@angular/core';
import { GlobalStatusService } from '../../services/global-status.service';
import { StyleService } from '../../services/style.service';
import htmlToImage from 'html-to-image';

@Component({
  selector: 'app-card-creation',
  templateUrl: './card-creation.component.html',
  styleUrls: ['./card-creation.component.scss']
})
export class CardCreationComponent {
  image: string | ArrayBuffer = '';

  constructor(public status: GlobalStatusService, public styles: StyleService) {}

  public imageSelected(image: File): void {
    const reader = new FileReader();
    reader.readAsDataURL(image);
    reader.onload = () => {
      this.image = reader.result;
    };
  }

  public save(element: any): void {
    htmlToImage.toPng(element).then((dataUrl) => {
      const link = document.createElement('a');
      link.href = dataUrl;
      link.download = '';
      link.click();
    }).catch((error) => {
      console.error('oops, something went wrong!', error);
    });
  }
}
