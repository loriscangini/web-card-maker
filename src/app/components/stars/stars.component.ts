import { Component, OnInit } from '@angular/core';
import { GlobalStatusService } from 'src/app/services/global-status.service';

@Component({
  selector: 'app-stars',
  templateUrl: './stars.component.html',
  styleUrls: ['./stars.component.scss']
})
export class StarsComponent implements OnInit {

  numbers = Array(13).fill(1).map((_, i) => i + 1);
  level = 4;

  constructor(public status: GlobalStatusService) { }

  ngOnInit(): void {
  }

}
