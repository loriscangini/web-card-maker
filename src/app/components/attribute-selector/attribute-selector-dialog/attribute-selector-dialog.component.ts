import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { GlobalStatusService } from 'src/app/services/global-status.service';

@Component({
  selector: 'app-attribute-selector-dialog',
  templateUrl: './attribute-selector-dialog.component.html',
  styleUrls: ['../attribute-selector.component.scss']
})
export class AttributeSelectorDialogComponent {
  attributes = ['Dark', 'Divine', 'Earth', 'Fire', 'Light', 'Water', 'Wind', 'Spell', 'Trap'];

  constructor(
    public dialogRef: MatDialogRef<AttributeSelectorDialogComponent>,
    public status: GlobalStatusService
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
