import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { GlobalStatusService } from 'src/app/services/global-status.service';
import { AttributeSelectorDialogComponent } from './attribute-selector-dialog/attribute-selector-dialog.component';

@Component({
  selector: 'app-attribute-selector',
  templateUrl: './attribute-selector.component.html',
  styleUrls: ['./attribute-selector.component.scss']
})
export class AttributeSelectorComponent {
  activeAttribute = 'Dark';

  constructor(public status: GlobalStatusService, public dialog: MatDialog) {}

  openDialog(): void {
    const dialogRef = this.dialog.open(AttributeSelectorDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.activeAttribute = result;
      }
    });
  }
}
