import { Component } from '@angular/core';
import { GlobalStatusService } from 'src/app/services/global-status.service';

@Component({
  selector: 'app-options-general',
  templateUrl: './options-general.component.html',
  styleUrls: ['./options-general.component.scss']
})
export class OptionsGeneralComponent {
  // public selectedSeries = 'Series 10';
  series = ['Series 9', 'Series 10'];

  // public selectedCardType = 'Effect';
  cardTypes = [
    {
      name: 'Official Monsters',
      values: ['Normal', 'Effect', 'Ritual', 'Fusion', 'Synchro', 'Xyz', 'Link']
    }, {
      name: 'Anime Monsters',
      values: ['Obelisk', 'Slifer', 'Ra', 'Dark Synchro', 'Z-Arc'],
    }, {
      name: 'Official Spell & Traps',
      values: ['Spell', 'Trap'],
    }
  ];

  constructor(public status: GlobalStatusService) {}
}
