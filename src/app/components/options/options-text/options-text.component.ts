import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-options-text',
  templateUrl: './options-text.component.html',
  styleUrls: ['./options-text.component.scss']
})
export class OptionsTextComponent {
  @Input() title = 'Text Options';
  @Input() showSingleLineOption = true;
  @Input() scaleX = 1;
  @Input() marginX = 0;
  @Input() marginY = 0;
  public isSingleLine = true;

  formatLabel(value: number): string {
    return Math.round(value * 100) + '%';
  }
}
