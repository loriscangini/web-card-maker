import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-options-image',
  templateUrl: './options-image.component.html',
  styleUrls: ['./options-image.component.scss']
})
export class OptionsImageComponent {

  @Input() title = 'Image Options';

  public selectedMode = 'Cover';
  modes = ['Contain', 'Cover', 'Fill', 'Scale-down', 'None'];

  verticalPosition = '50';
  horizontalPosition = '50';
  scale = 1;

  public get selectedPosition(): string {
    return this.horizontalPosition + '% ' + this.verticalPosition + '%';
  }

  formatLabel(value: number): string {
    return Math.round(value) + '%';
  }
}
