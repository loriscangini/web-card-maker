import { Component, Input, OnInit } from '@angular/core';
import FroalaEditor from 'froala-editor';
import { GlobalStatusService } from '../../services/global-status.service';

@Component({
  selector: 'app-froala-wrapper',
  templateUrl: './froala-wrapper.component.html',
  styleUrls: ['./froala-wrapper.component.scss']
})
export class FroalaWrapperComponent implements OnInit {

  constructor(public status: GlobalStatusService) {}

  get actualWidth(): string {
    return typeof this.width === 'string' ? this.width.toString() : ((+this.width / this.scaleX) + 'px');
  }

  static activeComponent: FroalaWrapperComponent = undefined;

  @Input() placeholder: string = undefined;
  @Input() transformOrigin = '0';
  @Input() scaleX = 1;
  @Input() width: number | string = 'auto';

  @Input() options: any = {
    pluginsEnabled: ['align', 'colors', 'emoticons', 'fontAwesome', 'fontFamily', 'fontSize', 'inlineStyle',
      'lineBreaker', 'lineHeight', 'lists', 'specialCharacters'],
    placeholderText: '',
    toolbarInline: true,
    toolbarVisibleWithoutSelection: true,
    fontFamily: {
      MatrixBook: 'Matrix Book',
      MatrixSmallCaps: 'Matrix Regular Small Caps',
      StoneSerif: 'Stone Serif',
      StoneSerifSmallCaps: 'Stone Serif Small Caps',
      'Arial, Helvetica, sans-serif': 'Arial',
      'Georgia, serif': 'Georgia',
      'Impact, Charcoal, sans-serif': 'Impact',
      'Tahoma, Geneva, sans-serif': 'Tahoma',
      'Times New Roman, Times, serif': 'Times New Roman',
      'Verdana, Geneva, sans-serif': 'Verdana'
    },
    fontSize: Array(99).fill(1).map((_, i) => (i + 1).toString()),
    inlineClasses: {

    },
    inlineStyles: {
      'Card Name': 'font-size: 60px; font-family: MatrixSmallCaps;',
      '[Type/Ability]': 'font-size: 20px; font-family: StoneSerifSmallCaps; font-weight: bold;',
      Effect: 'font-size: 16px; font-family: MatrixBook;',
      Lore: 'font-size: 16px; font-family: StoneSerif; font-style: italic;',
    },
    toolbarButtons: {
      moreText: {
        buttons: ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'insertRuby', 'fontFamily', 'fontSize', 'textColor', 'backgroundColor', 'inlineStyle', 'clearFormatting'],
        buttonsVisible: 0
      },
      moreParagraph: {
        buttons: ['alignLeft', 'alignCenter', 'alignRight', 'alignJustify', 'formatOL', 'formatUL', 'lineHeight', 'outdent', 'indent'],
        buttonsVisible: 0
      },
      moreRich: {
        buttons: ['insertSTSymbol', 'emoticons', 'fontAwesome', 'specialCharacters', 'insertHR'],
        buttonsVisible: 0
      }
    },
    events: {
      click : () => {
        FroalaWrapperComponent.activeComponent = this;
      }
    }
  };

  editor = undefined;
  ngOnInit(): void {
    FroalaEditor.DefineIcon('insertSTSymbol', {
      NAME: 'insertSTSymbol',
      SVG_KEY: `insertImage`
    });
    FroalaEditor.DefineIcon('insertRuby', {
      NAME: 'insertRuby',
      PATH: `M 22.796875 20.113281 C 21.820312 19.285156 20.503906 18.929688 19.40625 18.308594 C 17.542969 17.253906
        15.425781 16.207031 13.746094 14.878906 C 9.585938 11.589844 9.804688 7.675781 10.386719 2.867188 C 10.503906
        1.902344 10.214844 1.039062 9.398438 0.457031 C 8.28125 -0.34375 6.867188 -0.125 6.582031 1.382812 C 6.195312
        3.40625 7.398438 6.007812 7.539062 8.070312 C 7.703125 10.457031 7.457031 12.878906 6.613281 15.121094
        C 6.011719 16.710938 5.003906 18.316406 3.914062 19.621094 C 3.257812 20.40625 -0.382812 22.5 0.890625 23.726562
        C 2.195312 24.980469 5.679688 21.574219 6.515625 20.734375 C 6.886719 20.355469 8.914062 17.21875 9.824219 15.476562
        C 10.15625 14.839844 10.339844 14.390625 10.242188 14.328125 C 10.304688 14.371094 10.355469 14.40625 10.394531 14.453125
        C 12.1875 16.699219 13.738281 19.339844 15.902344 21.25 C 17.359375 22.53125 19.199219 22.296875 20.96875 22.039062
        C 22.097656 21.875 24.476562 21.53125 22.796875 20.113281 Z M 22.796875 20.113281
        M 12.335938 11.484375 C 12.691406 11.453125 13.019531 11.304688 13.324219 11.097656 C 14.945312 9.988281 16.566406 8.867188
        17.917969 7.410156 C 18.234375 7.070312 18.523438 6.6875 18.75 6.277344 C 19.152344 5.542969 19.027344 4.984375 18.390625 4.429688
        C 17.84375 3.957031 17.179688 3.785156 16.476562 3.714844 C 15.34375 3.605469 14.480469 4.164062 14.15625 5.25 C 14.078125 5.511719
        14.023438 5.785156 13.980469 6.054688 C 13.820312 7.03125 13.460938 7.921875 12.902344 8.734375 C 12.480469 9.351562 12.054688 9.964844
        11.644531 10.589844 C 11.578125 10.691406 11.519531 10.804688 11.464844 10.921875 C 11.371094 11.109375 11.742188 11.539062 12.335938
        11.484375 Z M 12.335938 11.484375
        M 1.878906 9.855469 C 2.691406 10.363281 3.117188 11.078125 3.410156 11.941406 C 3.523438 12.269531 3.710938 12.636719 3.976562 12.832031
        C 4.472656 13.191406 5.050781 13.199219 5.628906 12.910156 C 5.929688 12.761719 6.175781 12.578125 6.355469 12.34375 C 6.664062 11.933594
        6.597656 11.269531 6.519531 11.007812 C 6.472656 10.851562 6.421875 10.695312 6.347656 10.550781 C 5.5 8.90625 3.769531 8.582031 2.285156
        7.898438 C 2.164062 7.84375 1.894531 7.953125 1.78125 8.066406 C 1.339844 8.519531 1.355469 9.527344 1.878906 9.855469 Z M 1.878906 9.855469`
      });

    FroalaEditor.RegisterCommand('insertSTSymbol', {
      title: 'Insert Symbols',
      type: 'dropdown',
      focus: true,
      undo: true,
      refreshAfterCallback: true,
      options: {
        Continuous: 'Continuous',
        Counter: 'Counter',
        Equip: 'Equip',
        Field: 'Field',
        'Quick-Play': 'Quick-Play',
        Ritual: 'Ritual'
      },
      callback: (_, v) => {
        FroalaWrapperComponent.activeComponent.editor.html.insert(
          '<img class="symbol" src="/assets/' + this.status.series + '/Symbols/' + v + '.png">'
        );
      }
    });

    FroalaEditor.RegisterCommand('insertRuby', {
      title: 'Insert Ruby',
      focus: true,
      undo: true,
      refreshAfterCallback: true,
      callback: () => {
        let base = FroalaWrapperComponent.activeComponent.editor.selection.get();
        if (FroalaWrapperComponent.activeComponent.editor.selection.text() === '') {
          base = 'base';
        }
        FroalaWrapperComponent.activeComponent.editor.html.insert(
          '<ruby>' + base + '<rt>Ruby</rt></ruby><span> </span>'
        );
      }
    });
  }

  public initialize(initControls): void {
    initControls.initialize();
    this.editor = initControls.getEditor();
 }
}
