import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CardCreationComponent } from './components/card-creation/card-creation.component';

const routes: Routes = [
  { path: '', component: CardCreationComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
