import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GlobalStatusService {
  public series = 'Series 10';
  public cardType = 'Effect';
  public isPendulum = false;
  public isDeckMaster = false;
  public boxSize: 'Large' | 'Medium' | 'Small' = 'Large';
  public smallImage = false;

  public linkArrows = new Map<'topleft' | 'top' | 'topright' | 'left' | 'right' | 'bottomleft' | 'bottom' | 'bottomright', boolean> ([
    ['topleft', false], ['top', false], ['topright', false], ['left', false], ['right', false], ['bottomleft', false], ['bottom', false], ['bottomright', false]
  ]);

  public get isPendulumOrDeckMaster(): boolean {
     return this.isPendulum || this.isDeckMaster;
  }

  public get isMonster(): boolean {
    return this.cardType !== 'Spell' && this.cardType !== 'Trap';
  }
  public get starType(): string {
    if (!this.isMonster) {
      return undefined;
    }
    switch (this.cardType) {
      case 'Xyz': return 'Rank';
      case 'Link': return undefined;
      case 'Dark Synchro': return 'Neglevel';
      default: return 'Level';
    }
  }
  public get starPosition(): 'left' | 'right' {
    switch (this.starType) {
      case 'Rank' : return 'left';
      case 'Neglevel' : return 'left';
      default: return 'right';
    }
  }
  public get starUrl(): string {
    return '/assets/' + this.series + '/Stars/' + this.starType + '.png';
  }
}
