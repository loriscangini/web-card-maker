import { Injectable } from '@angular/core';
import { GlobalStatusService } from './global-status.service';

@Injectable({
  providedIn: 'root'
})
export class StyleService {

  get artworkContainer(): Style {
    if (!this.status.isPendulumOrDeckMaster) {
      return {
        top: '146px',
        left: '66px',
        right: '64px',
        height: '418px'
      };
    } else {
      return {
        top: '142px',
        left: '37px',
        right: '35px',
        height: '474px'
      };
    }
  }
  get artwork(): Style {
    if (this.status.isPendulumOrDeckMaster) {
      let maxHeight = '100%';
      if (this.status.smallImage) {
        if (this.status.boxSize === 'Small') {
          maxHeight = '386px';
        } else {
          maxHeight = '358px';
        }
      }
      return {
        maxHeight
      };
    }
  }
  public linkArrow(dir: 'topleft' | 'top' | 'topright' | 'left' | 'right' | 'bottomleft' | 'bottom' | 'bottomright'): Style {
    switch (dir) {
      case 'topleft':
        return {
          left: '47px',
          top: '127px'
        };
      case 'top':
        return {
          left: '226px',
          top: '116px'
        };
      case 'topright':
        return {
          right: '44px',
          top: '127px'
        };
      case 'left':
        return {
          left: '35px',
          top: '307px'
        };
      case 'right':
        return {
          right: '33px',
          top: '307px'
        };
      case 'bottomleft':
        return {
          left: '47px',
          top: '532px'
        };
      case 'bottom':
        return {
          left: '226px',
          top: '564px'
        };
      case 'bottomright':
        return {
          right: '44px',
          top: '532px'
        };
    }
  }

  get setCode(): Style {
    if (this.status.isPendulumOrDeckMaster) {
      return {
        left: '46px',
        top: '728px'
      };
    } else {
      return {
        top: '571px',
        right: '57px'
      };
    }
  }

  get pendulumEffect(): Style {
    if (this.status.isPendulum) {
      if (this.status.boxSize !== 'Small') {
        return {
          left: '87px',
          top: '505px'
        };
      } else {
        return {
          left: '87px',
          top: '531px'
        };
      }
    } else {
      if (this.status.boxSize !== 'Small') {
        return {
          left: '46px',
          top: '505px'
        };
      } else {
        return {
          left: '46px',
          top: '531px'
        };
      }
    }
  }

  get leftScale(): Style {
    switch (this.status.boxSize) {
      case 'Small':
      return {
        left: '43px',
        top: '569px'
      };
      case 'Medium':
      return {
        left: '43px',
        top: '558px'
      };
      case 'Large':
      return {
        left: '43px',
        top: '567px'
      };
    }
  }
  get rightScale(): Style {
    switch (this.status.boxSize) {
      case 'Small':
      return {
        right: '39px',
        top: '569px'
      };
      case 'Medium':
      return {
        right: '39px',
        top: '558px'
      };
      case 'Large':
      return {
        right: '39px',
        top: '567px'
      };
    }
  }

  get typeAbility(): Style {
    if (this.status.isMonster) {
      if (this.status.isPendulum && this.status.boxSize === 'Large') {
        return {
          top: '616px',
          left: '46px',
          fontSize: '20px'
        };
      } else {
        return {
          top: '595px',
          left: '46px',
          fontSize: '20px'
        };
      }
    } else {
      return {
        top: '97px',
        right: '58px',
        fontSize: '23px'
      };
    }
  }

  get effect(): Style {
    if (this.status.isMonster) {
      if (this.status.isPendulum && this.status.boxSize === 'Large') {
        return {
          top: '645px',
          left: '46px'
        };
      } else {
        return {
          top: '624px',
          left: '46px'
        };
      }
    } else {
      return {
        top: '600px',
        left: '46px'
      };
    }
  }

  constructor(public status: GlobalStatusService) { }
}

export class Style {
  [property: string]: string;
}
