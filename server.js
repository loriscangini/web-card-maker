const express = require('express');
const app = express();

app.use(express.static('./dist/Web-Card-Maker'));
app.get('/*', function(req, res) {
  res.sendFile('index.html', {root: 'dist/Web-Card-Maker/'});
});
app.listen(process.env.PORT || 80);